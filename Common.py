"""

//[]------------------------------------------------------------------------[]
//|                                                                          |
//|                              Common Module                               |
//|                               Version 1.0                                |
//|                                                                          |
//|              Copyright 2015-2020, Marcos Vinicius Teixeira               |
//|                          All Rights Reserved.                            |
//|                                                                          |
//[]------------------------------------------------------------------------[]
//
OVERVIEW: feature_extraction.py
//  ================================
//  This module implement methods that are common to another modules, by exam-
//  the feature_extraction.py.
//
"""

from os.path import exists, isdir, basename, isfile, join, splitext
from glob import glob

from keras.utils import np_utils
from numpy import zeros, resize, sqrt, histogram, hstack, vstack, savetxt, zeros_like, fromstring, asarray, array
import numpy as np
import scipy.cluster.vq as vq
import os
from os.path import splitext, exists
from PIL import Image, ImageDraw, ImageFont
import time

EXTENSIONS = ["jpg", "bmp", "png"]

# dict for UCF classes
targets_ucf = {}

class Common:

    @staticmethod
    def process_ucf_dataset(datasetpath):
        all_classes = []
        all_videos = []

        maxvideos_infolder = 5

        # Getting each class name
        for cl in glob(datasetpath + "/*"):
            all_classes.extend([join(datasetpath, basename(cl))])

        count_vid = 0
        # Getting each video name
        for i in range(len(all_classes)):
            for vid in glob(all_classes[i] + "/*"):

                if count_vid >= maxvideos_infolder:
                    break

                all_videos.extend([join(all_classes[i], basename(vid))])
                count_vid += 1
            count_vid = 0

        # Getting the frames for each video, using the script 'process_video.py'
        for vid in all_videos:
            cl = vid.split("/")[-2]
            # Taking the frames ..
            cmnd = "python process_video.py " + cl + ' ' + vid
            os.system(cmnd)
    @staticmethod
    def load_data(datasetpath):

        all_classes = []
        # getting the classes
        for cl in glob(datasetpath + "/*"):
            all_classes.extend([join(datasetpath, basename(cl))])

        all_train_videos = []
        all_test_videos = []
        train_frames=[]
        test_frames = []

        # Getting each video name
        aux_train_videos=[]
        aux_test_videos=[]
        videos=0
        for i in xrange(len(all_classes)):
            for vid in glob(all_classes[i] + "/*"):
                #leave-one-out
                if(videos < (len(glob(all_classes[i] + "/*"))-1)):
                    aux_train_videos.append(vid)
                    print("train",vid)
                else:
                    print("test", vid)
                    aux_test_videos.append(vid)
                videos+=1
            videos=0
            for i in aux_train_videos:
                all_train_videos.append(i)
            for i in aux_test_videos:
                all_test_videos.append(i)
            aux_train_videos=[]
            aux_test_videos=[]

        # train images
        for i in xrange(len(all_train_videos)):
            for frame in glob(all_train_videos[i] + "/*"):
                if (frame.split(".")[1] in EXTENSIONS and len(frame.split(".")) == 2):
                    train_frames.append(frame)

        # test images
        for i in xrange(len(all_test_videos)):
            for frame in glob(all_test_videos[i] + "/*"):
                if (frame.split(".")[1] in EXTENSIONS and len(frame.split(".")) == 2):
                    test_frames.append(frame)


        img_rows, img_cols = 32, 32

        X_train = np.zeros((len(train_frames), 3, img_rows, img_cols), dtype="uint8")
        X_test  = np.zeros((len(test_frames), 3, img_rows, img_cols), dtype="uint8")
        y_train = np.zeros((len(train_frames),), dtype="uint8")
        y_test  = np.zeros((len(test_frames),), dtype="uint8")

        for i in range(len(all_classes)):
            targets_ucf[i] = basename(all_classes[i])


        print("Extracting images ...")
        start = time.time()
        labels=[]

        image_names = []
        for index, im in enumerate(train_frames):
            image = Image.open(im)
            image.resize((img_rows,img_cols))
            image = image.load()
            pixels=[]
            for i in range(img_rows):
                for j in range(img_cols):
                    X_train[index,:,i,j] = image[i,j]

            for i in range(len(targets_ucf)):
                if train_frames[index].split("/")[1] == targets_ucf[i]:
                    y_train[index] = i
                    if targets_ucf[i] not in labels:
                        labels.append(targets_ucf[i])

                    break

        for index, im in enumerate(test_frames):
            image_names.append(im)
            image = Image.open(im)
            image.resize((img_rows, img_cols))
            image = image.load()
            pixels = []
            for i in range(img_rows):
                for j in range(img_cols):
                    X_test[index, :, i, j] = image[i, j]

            for i in range(len(targets_ucf)):
                if test_frames[index].split("/")[1] == targets_ucf[i]:
                    y_test[index] = i
                    if targets_ucf[i] not in labels:
                        labels.append(targets_ucf[i])

                    break

        end = time.time()
        print ("tempo gasto: ",int(end-start), " segundos")
        #y = np.bincount(y_test)
        #ii = np.nonzero(y)[0]
        #print(zip(ii,y[ii]))

        return X_train, X_test, y_train ,y_test, image_names, labels


    # extracting the class names given a folder name (dataset)
    @staticmethod
    def get_classes(datasetpath):
        cat_paths = [files
                     for files in glob(datasetpath + "/*")
                     if isdir(files)]

        cat_paths.sort()
        cats = [basename(cat_path) for cat_path in cat_paths]

        return cats


    # getting the array of files(images) inside a given folder
    @staticmethod
    def get_imgfiles(path):
        all_files = []

        all_files.extend([join(path, basename(fname))
                          for fname in glob(path + "/*")
                          if splitext(fname)[-1].lower() in EXTENSIONS])
        return all_files
