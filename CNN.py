from Common import Common
from keras.models import Sequential,Model
from keras.layers.core import Dense, Dropout, Activation, Flatten, Reshape, RepeatVector
from keras.layers.convolutional import Convolution2D, MaxPooling2D
from sklearn.metrics import accuracy_score, confusion_matrix
from sklearn.cross_validation import  train_test_split, ShuffleSplit
from keras.layers.recurrent import LSTM
from keras.layers.normalization import BatchNormalization
from keras.optimizers import SGD
from keras.utils import np_utils
from os.path import exists
import numpy as np
import cv2

batch_size = 100
nb_classes = 9
nb_epoch = 10

# input image dimensions
img_rows, img_cols = 100, 100

img_channels = 3

X, y , image_names, labels = Common().load_data("frames")

cv = ShuffleSplit(len(X), test_size=0.3,
                  train_size=0.7,
                  random_state=42)

train,test = next(iter(cv))

X_train = []
y_train = []
X_test = []
y_test = []

test_index=[]

for i in train:
    X_train.append(X[i,:,:,:])
    y_train.append(y[i])

for i in test:
    X_test.append(X[i,:,:,:])
    y_test.append(y[i])
    test_index.append(i)

X_train = np.array(X_train)
X_test = np.array(X_test)
y_train = np.array(y_train)
y_test = np.array(y_test)

print(len(X_train))
print(len(X_test))

y_train = np_utils.to_categorical(y_train, nb_classes)
y_test = np_utils.to_categorical(y_test, nb_classes)

visual_model = Sequential()

visual_model.add(Convolution2D(32, 3, 3, border_mode='same',
                        input_shape=(img_channels, img_rows, img_cols)))
visual_model.add(Activation('relu'))
visual_model.add(Convolution2D(32, 3, 3))
visual_model.add(Activation('relu'))
visual_model.add(MaxPooling2D(pool_size=(2, 2)))
visual_model.add(Dropout(0.25))

visual_model.add(Convolution2D(64, 3, 3, border_mode='same'))
visual_model.add(Activation('relu'))
visual_model.add(Convolution2D(64, 3, 3))
visual_model.add(Activation('relu'))
visual_model.add(MaxPooling2D(pool_size=(2, 2)))
visual_model.add(Dropout(0.25))

visual_model.add(Flatten())
visual_model.add(Dense(512))
visual_model.add(Activation('relu'))
visual_model.add(Dropout(0.5))
visual_model.add(Dense(nb_classes))
visual_model.add(Activation('softmax'))

visual_model.compile(loss='categorical_crossentropy', optimizer='rmsprop', metrics=["accuracy"])

X_train = X_train.astype('float32')
X_test = X_test.astype('float32')
X_test /= 255
X_train /= 255

if not exists("weights.hdf5"):
    visual_model.fit(X_train, y_train, batch_size=batch_size,
              nb_epoch=nb_epoch,
              validation_data=(X_test, y_test), shuffle=True)
    visual_model.save_weights("weights.hdf5")
else:
    print("carregando pesos!")
    visual_model.load_weights("weights.hdf5")

pred_classes = visual_model.predict_classes(X_test)

#target = []
#for index, i in enumerate(y_test):
#    target.append(np.argmax(i))

#print(confusion_matrix(target,pred_classes))
#print("accuracy : ", accuracy_score(target,pred_classes))


# visualizing
 
count=0
for index in range(len((X_test))):

    im = cv2.imread(image_names[test_index[index]])
    prediction = pred_classes[index]
    target     = np.argmax(y_test[index])
    height, width, channels = im.shape

    cv2.putText(im, "predicted: " + str(labels[prediction]), (height/2, width/4), cv2.FONT_HERSHEY_SIMPLEX, 0.8, (255, 255, 255), 1, cv2.LINE_AA)
    cv2.putText(im, "target: " + str(labels[target]), (height/2, width/2), cv2.FONT_HERSHEY_SIMPLEX, 0.8, (255, 255, 255), 1, cv2.LINE_AA)
    #cv2.imwrite("new"+str(count)+".jpg",im)
    cv2.namedWindow("imagem")
    cv2.imshow("imagem",im)
    cv2.waitKey(0)
    #print("foto: ", image_names[i])
    #print("prediction: ",pred_classes[i])
    #print("target: ",y_test[i])
    count+=1
